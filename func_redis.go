package main

import (
	"fmt"

	"github.com/garyburd/redigo/redis"
)

// Estrutura Mensagem
type Mensagem struct {
	// <TIMEGRAMA> 3 - COMPLETAR A ESTRUTURA MENSAGEM </TIMEGRAMA>

	Msg string `json:"msg,omitempty"`
}

// Estrutura Erro
type Erro struct {
	Msg string `json:"msg,omitempty"`
}

var erros []Erro
var msgs []Mensagem

// ValidarChaveValor
func ValidarChaveValor(pC string, pV string) bool {
	//Limpar erros
	erros = erros[:0]

	var verificar bool = true
	if len(pC) == 0 {
		erros = append(erros, Erro{Msg: ("Parametro chave não foi informado.: " + string(pC))})
		verificar = false
	}

	// <TIMEGRAMA> 4 - CORRGIR A VALIDACAO DA MENSAGEM NA PUBLICACAO </TIMEGRAMA>
	erros = append(erros, Erro{Msg: ("Parametro valor não foi informado.: " + string(pV))})
	verificar = false
	// <TIMEGRAMA> 4 - CORRGIR A VALIDACAO DA MENSAGEM NA PUBLICACAO </TIMEGRAMA>
	return verificar
}

// ValidarCanal
func ValidarCanal(pCGet string) bool {
	//Limpar redis
	erros = erros[:0]

	var verificar bool = true
	if len(pCGet) == 0 {
		erros = append(erros, Erro{Msg: ("Parametro canal não foi informado.: " + string(pCGet))})
		verificar = false
	}
	return verificar
}

//InsereRedis Insere no banco redis
func Set(pCGet string, pVGet string) {
	var pool = newPool()
	c := pool.Get()
	c.Send("SET", string(pCGet), string(pVGet))
	defer c.Close()
}

// Publish publish chave valor
func Publish(chave string, valor string) {
	var pool = newPool()
	c := pool.Get()
	// <TIMEGRAMA> 5 - INFORMAR A FUNÇÂO PUBLISH PARA O REDIS </TIMEGRAMA>
	c.Do("", chave, valor)
	defer c.Close()
}

// Subscribe
func Subscribe(key string) {
	erros = erros[:0]

	var pool = newPool()
	c := pool.Get()

	psc := redis.PubSubConn{Conn: c}
	if err := psc.Subscribe(key); err != nil {
		erros = append(erros, Erro{Msg: ("Ocorreu um erro interno, por favor entre em contato com o ADM do sistema.")})
	}
	for {
		switch v := psc.Receive().(type) {
		case redis.Message:
			msgs = append(msgs, Mensagem{Canal: v.Channel, Msg: fmt.Sprintf("%s", v.Data)})
			fmt.Printf("%s: message: %s\n", v.Channel, v.Data)
			//defer c.Close()
			//return
		}
	}
}

// conexao
func newPool() *redis.Pool {
	return &redis.Pool{
		MaxIdle:   80,
		MaxActive: 12000, // numero maximo de conexao
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", fmt.Sprintf("%s:%s", "54.144.7.89", "6379"))
			if err != nil {
				panic(err.Error())
			}
			c.Do("AUTH", "techtalk")
			return c, err
		},
	}
}

//c, err := redis.Dial("tcp", fmt.Sprintf("%s:%s", "192.168.43.38", "6379"))
