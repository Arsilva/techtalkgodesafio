FROM golang:alpine
RUN apk add git
RUN go get github.com/gorilla/mux
RUN go get github.com/garyburd/redigo/redis
RUN go get github.com/gorilla/handlers
WORKDIR /go/src
EXPOSE 8081
COPY ./main.go /go/src/main.go
COPY ./func_redis.go /go/src/func_redis.go
RUN go build main.go func_redis.go
CMD ["./main"]