package main

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// função principal
func main() {
	go Subscribe("grama")

	router := mux.NewRouter()

	allowedHeaders := handlers.AllowedHeaders([]string{"X-Requested-With", "Content-Type"})
	allowedOrigins := handlers.AllowedOrigins([]string{"*"})
	allowedMethods := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS"})

	router.HandleFunc("/setredis", SetRedis).Methods("GET")
	router.HandleFunc("/publishredis", PublishRedis).Methods("POST")
	// <TIMEGRAMA> 1 - COMPLETAR A ROTA </TIMEGRAMA>
	router.HandleFunc("/subscriberedis").Methods("GET")

	log.Fatal(http.ListenAndServe(":8081", handlers.CORS(allowedHeaders, allowedOrigins, allowedMethods)(router)))
}

// InsertRedis Insere no redis
func SetRedis(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		erros = append(erros, Erro{Msg: ("Ocorreu um erro interno, por favor entre em contato com o ADM do sistema.")})
	}

	if r.Method == "GET" {
		pVGet := r.Form.Get("valor")
		pCGet := r.Form.Get("chave")

		if ValidarChaveValor(pCGet, pVGet) {
			Set(pCGet, pVGet)
		} else {
			json.NewEncoder(w).Encode(erros)
		}
	}
}

// publicarPost publica no canal
func PublishRedis(w http.ResponseWriter, r *http.Request) {
	var msg []Mensagem
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Accept-Charset", "utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")

	var p Mensagem
	_ = json.NewDecoder(r.Body).Decode(&p)

	if ValidarChaveValor(p.Canal, p.Msg) {
		// <TIMEGRAMA> 2 - ADICIONAR PARAMETRO PUBLICACAO </TIMEGRAMA>
		Publish(p.Canal)
		msg = append(msg, p)
		json.NewEncoder(w).Encode(msg)
	} else {
		json.NewEncoder(w).Encode(erros)
	}
}

// SubscribeRedis
func SubscribeRedis(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		erros = append(erros, Erro{Msg: ("Ocorreu um erro interno, por favor entre em contato com o ADM do sistema.")})
	}
	json.NewEncoder(w).Encode(msgs)
}
